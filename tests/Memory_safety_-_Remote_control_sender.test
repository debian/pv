#!/bin/sh
#
# Run valgrind's memory checker while using remote control to control
# another process.

# Allow all tests to be skipped, e.g. during a release build
test "${SKIP_ALL_TESTS}" = "1" && exit 77

true "${sourcePath:?not set - call this from 'make check'}"
true "${testSubject:?not set - call this from 'make check'}"
true "${workFile1:?not set - call this from 'make check'}"
true "${workFile2:?not set - call this from 'make check'}"
true "${workFile3:?not set - call this from 'make check'}"
true "${workFile4:?not set - call this from 'make check'}"

# Do nothing if it is not supported.
if ! "${testSubject}" -h 2>/dev/null | grep -Eq "^  -R,"; then
	echo "Not supported on this platform"
	exit 77
fi

# Load the valgrind function.
. "${sourcePath}/tests/run-valgrind.sh"

dd if=/dev/urandom of="${workFile1}" bs=1024 count=10240 2>/dev/null

# Check from the POV of the process sending a control command.
true > "${workFile3}"
"${testSubject}" -L 100k -i 0.1 -f -P "${workFile3}" "${workFile1}" > "${workFile2}" 2>/dev/null &
while test -e "${workFile3}" && ! test -s "${workFile3}"; do usleep 200000 2>/dev/null || sleep 1; done
runWithValgrind -R "$(cat "${workFile3}")" -apterb 4>&1   || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -p 4>&1        || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -N "test" 4>&1 || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -F "%e" 4>&1   || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -x "window:%b" 4>&1   || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -N "."  4>&1   || exit 1
(usleep 200000 || sleep 1) 2>/dev/null
runWithValgrind -R "$(cat "${workFile3}")" -L 10M 4>&1    || exit 1
wait

exit 0
