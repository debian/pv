#!/bin/sh
#
# Test that "make install" installs the expected minimum set of files.

# Allow all tests to be skipped, e.g. during a release build
test "${SKIP_ALL_TESTS}" = "1" && exit 77

true "${testSubject:?not set - call this from 'make check'}"
true "${workFile1:?not set - call this from 'make check'}"

if test "${XCTEST}" = "1"; then
	echo "cross-compile flag set - skipping as local make will not run"
	exit 77
fi

makeDir="${testSubject%/*}"
if ! test -d "${makeDir}"; then
	echo "could not find the directory to run \`make' in"
	exit 77
fi

makeCommand=$(command -v gmake 2>/dev/null)
test -z "${makeCommand}" && makeCommand="make"

testInstallDir=$(mktemp -d)
if ! test -n "${testInstallDir}"; then
	echo "failed to create temporary installation directory"
	exit 77
fi

export DESTDIR="${testInstallDir}"
${makeCommand} -C "${makeDir}" install DESTDIR="${testInstallDir}" >"${workFile1}" 2>&1
rc=$?
if ! test "${rc}" -eq 0; then
	if ! test "${makeCommand}" = "make"; then
		# Retry with plain "make".
		makeCommand="make"
		${makeCommand} -C "${makeDir}" install DESTDIR="${testInstallDir}" >>"${workFile1}" 2>&1
		rc=$?
	fi
fi
if ! test ${rc} -eq 0; then
	echo "\`make install' exited ${rc}"
	echo "command: ${makeCommand} -C '${makeDir}' install DESTDIR='${testInstallDir}'"
	cat "${workFile1}"
	rm -rf "${testInstallDir}"
	exit 1
fi

exitStatus=0
if ! find "${testInstallDir}" -type f -name "pv.1*" | grep -Fq "pv.1"; then
	echo "installation of man page failed"
	exitStatus=1
fi
if ! find "${testInstallDir}" -type f -name "pv*" | grep -Fv "pv.1" | grep -Fq "pv"; then
	echo "installation of binary failed"
	exitStatus=1
fi

rm -rf "${testInstallDir}"
exit "${exitStatus}"
